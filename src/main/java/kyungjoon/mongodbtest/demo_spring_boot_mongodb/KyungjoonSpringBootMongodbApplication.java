package kyungjoon.mongodbtest.demo_spring_boot_mongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KyungjoonSpringBootMongodbApplication {

    public static void main(String[] args) {

        SpringApplication.run(KyungjoonSpringBootMongodbApplication.class, args);
    }

}
