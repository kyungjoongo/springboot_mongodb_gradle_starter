package kyungjoon.mongodbtest.demo_spring_boot_mongodb.configs;

import kyungjoon.mongodbtest.demo_spring_boot_mongodb.interceptors.InterceptorOne;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class AppConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new InterceptorOne());
    }

}
