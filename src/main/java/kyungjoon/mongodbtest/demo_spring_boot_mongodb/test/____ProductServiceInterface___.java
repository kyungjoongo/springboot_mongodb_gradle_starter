package kyungjoon.mongodbtest.demo_spring_boot_mongodb.test;

import kyungjoon.mongodbtest.demo_spring_boot_mongodb.features.product.Product;

import java.util.List;


public interface ____ProductServiceInterface___ {
    Product createProduct(Product product);

    Product updateProduct(Product product) throws Exception;

    List<Product> getAllProduct();

    Product getProductById(long productId) throws Exception;

    void deleteProduct(long id) throws Exception;
}
