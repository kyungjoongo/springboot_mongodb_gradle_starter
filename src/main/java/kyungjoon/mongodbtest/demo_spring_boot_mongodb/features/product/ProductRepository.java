package kyungjoon.mongodbtest.demo_spring_boot_mongodb.features.product;

import org.springframework.data.mongodb.repository.MongoRepository;


public interface ProductRepository extends MongoRepository<Product, Long> {

}
