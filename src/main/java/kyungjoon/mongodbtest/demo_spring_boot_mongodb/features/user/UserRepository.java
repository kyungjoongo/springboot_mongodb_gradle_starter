package kyungjoon.mongodbtest.demo_spring_boot_mongodb.features.user;

import org.springframework.data.mongodb.repository.MongoRepository;


public interface UserRepository extends MongoRepository<User, String> {

}
