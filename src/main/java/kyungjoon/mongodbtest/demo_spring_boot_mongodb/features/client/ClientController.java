package kyungjoon.mongodbtest.demo_spring_boot_mongodb.features.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class ClientController {

    @Autowired
    private ClientService clientService;

    @GetMapping("/clients")
    public Object getAllProduct() {
        List<Client> clients = clientService.getAllProduct();
        try {
            return ResponseEntity.ok().body(clients);
        } catch (Exception e) {
            return ResponseEntity.badRequest().toString();
        }


    }

    @GetMapping("/clients/{id}")
    public ResponseEntity<Client> getProductById(@PathVariable long id) throws Exception {
        return ResponseEntity.ok().body(clientService.getProductById(id));
    }

    @PostMapping("/clients")
    public ResponseEntity<Client> createProduct(@RequestBody Client client) {

        System.out.println("sdlfksdlkflsdkfksdfk====>>>>>");

        return ResponseEntity.ok().body(this.clientService.createProduct(client));
    }

    @PutMapping("/clients/{id}")
    public ResponseEntity<Client> updateProduct(@PathVariable long id, @RequestBody Client client) throws Exception {
        client.setId(id);
        return ResponseEntity.ok().body(this.clientService.updateProduct(client));
    }

    @DeleteMapping("/clients/{id}")
    public HttpStatus deleteProduct(@PathVariable long id) throws Exception {
        this.clientService.deleteProduct(id);
        return HttpStatus.OK;
    }
}
