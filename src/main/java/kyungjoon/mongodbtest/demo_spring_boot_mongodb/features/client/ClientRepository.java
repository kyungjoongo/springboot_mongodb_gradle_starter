package kyungjoon.mongodbtest.demo_spring_boot_mongodb.features.client;

import org.springframework.data.mongodb.repository.MongoRepository;


public interface ClientRepository extends MongoRepository<Client, Long> {

}
