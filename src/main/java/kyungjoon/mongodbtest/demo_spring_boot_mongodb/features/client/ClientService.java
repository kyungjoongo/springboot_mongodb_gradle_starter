package kyungjoon.mongodbtest.demo_spring_boot_mongodb.features.client;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ClientService {


    @Autowired
    public ClientRepository clientRepository;


    public Client createProduct(Client client) {

        Client savedClient = clientRepository.save(client);
        return savedClient;
    }

    public Client updateProduct(Client pClient) throws Exception {
        Optional<Client> clientDb = this.clientRepository.findById(pClient.getId());

        if (clientDb.isPresent()) {
            Client clientUpdate = clientDb.get();
            clientUpdate.setId(pClient.getId());
            clientUpdate.setName(pClient.getName());
            clientRepository.save(clientUpdate);
            return clientUpdate;
        } else {
            throw new Exception("Record not found with id : " + pClient.getId());
        }
    }

    public List<Client> getAllProduct() {

        List<Client> clients = this.clientRepository.findAll();

        return clients;
    }

    public Client getProductById(long id) throws Exception {

        Optional<Client> clientDb = this.clientRepository.findById(id);

        if (clientDb.isPresent()) {
            return clientDb.get();
        } else {
            throw new Exception("Record not found with id : " + id);
        }
    }

    public void deleteProduct(long clientId) throws Exception {
        Optional<Client> clientDb = this.clientRepository.findById(clientId);

        if (clientDb.isPresent()) {
            this.clientRepository.delete(clientDb.get());
        } else {
            throw new Exception("Record not found with id : " + clientId);
        }

    }
}
