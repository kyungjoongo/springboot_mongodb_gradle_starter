package kyungjoon.mongodbtest.demo_spring_boot_mongodb.features.login;

import kyungjoon.mongodbtest.demo_spring_boot_mongodb.features.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;


    @PostMapping("/login")
    public ResponseEntity<User> login(@RequestBody User user) throws Exception {

        User userOne = this.loginService.getUserOne(user);

        return ResponseEntity.ok().body(userOne);
    }


}
