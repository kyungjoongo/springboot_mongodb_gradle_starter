package kyungjoon.mongodbtest.demo_spring_boot_mongodb.features.login;


import kyungjoon.mongodbtest.demo_spring_boot_mongodb.features.user.User;
import kyungjoon.mongodbtest.demo_spring_boot_mongodb.features.user.UserRepository;
import kyungjoon.mongodbtest.demo_spring_boot_mongodb.utils.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.springframework.data.mongodb.core.query.Criteria.where;


@Service
@Transactional
public class LoginService {


    @Autowired
    public UserRepository userRepository;


    public User getUserOne(User paramUser) throws Exception {


        Optional<User> dbUser = this.userRepository.findById(paramUser.getId());


        if (dbUser.isPresent()) {
            System.out.println("getId===>" + dbUser.get().getId());
            System.out.println("getPwd===>" + dbUser.get().getPwd());

            String token = JwtUtil.makeJwtToken();

            System.out.println("token===>" + token);
            User _user =dbUser.get();
            _user.setAccessToken(token);
            return _user;

        } else {
            throw new Exception("Record not found with id : " + paramUser.getId());
        }
    }


}
