package kyungjoon.mongodbtest.demo_spring_boot_mongodb.features.member;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class MemberService {


    @Autowired
    public MemberRepository memberRepository;


    public Member create(Member member) {

        Member savedMember = memberRepository.save(member);
        return savedMember;
    }

    public Member update(Member pMember) throws Exception {
        Optional<Member> clientDb = this.memberRepository.findById(pMember.getId());

        if (clientDb.isPresent()) {
            Member memberUpdate = clientDb.get();
            memberUpdate.setId(pMember.getId());
            memberUpdate.setName(pMember.getName());
            memberRepository.save(memberUpdate);
            return memberUpdate;
        } else {
            throw new Exception("Record not found with id : " + pMember.getId());
        }
    }

    public List<Member> getAll() {

        List<Member> members = this.memberRepository.findAll();

        return members;
    }

    public Member getOneById(long id) throws Exception {

        Optional<Member> clientDb = this.memberRepository.findById(id);

        if (clientDb.isPresent()) {
            return clientDb.get();
        } else {
            throw new Exception("Record not found with id : " + id);
        }
    }

    public void deleteOne(long id) throws Exception {
        Optional<Member> clientDb = this.memberRepository.findById(id);

        if (clientDb.isPresent()) {
            this.memberRepository.delete(clientDb.get());
        } else {
            throw new Exception("Record not found with id : " + id);
        }

    }
}
