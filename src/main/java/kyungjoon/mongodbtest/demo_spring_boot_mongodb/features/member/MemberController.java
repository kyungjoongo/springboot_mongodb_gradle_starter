package kyungjoon.mongodbtest.demo_spring_boot_mongodb.features.member;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;






@RestController
public class MemberController {

    @Autowired
    private MemberService memberService;

    @GetMapping("/members")
    public ResponseEntity<List<Member>> getAllProduct() {

        List<Member> members = memberService.getAll();
        return ResponseEntity.ok().body(members);
    }

    @GetMapping("/members/{id}")
    public ResponseEntity<Member> getProductById(@PathVariable long id) throws Exception {
        return ResponseEntity.ok().body(memberService.getOneById(id));
    }

    @PostMapping("/members")
    public ResponseEntity<Member> createProduct(@RequestBody Member member) {


        return ResponseEntity.ok().body(this.memberService.create(member));
    }

    @PutMapping("/members/{id}")
    public ResponseEntity<Member> updateProduct(@PathVariable long id, @RequestBody Member member) throws Exception {
        member.setId(id);
        return ResponseEntity.ok().body(this.memberService.update(member));
    }

    @DeleteMapping("/members/{id}")
    public HttpStatus deleteProduct(@PathVariable long id) throws Exception {
        this.memberService.deleteOne(id);
        return HttpStatus.OK;
    }
}
