package kyungjoon.mongodbtest.demo_spring_boot_mongodb.features.member;

import org.springframework.data.mongodb.repository.MongoRepository;


public interface MemberRepository extends MongoRepository<Member, Long> {

}
